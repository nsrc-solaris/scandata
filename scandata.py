"""
This is main script of Scan Data project

To use this script one need to fulfill following requirements
* use Python 3.6
* install package pyyaml from pip (`pip install pyyaml` executed from command line)

To run this script, type `python -m scandata` in command line

To get help on how to use this script, provide `-h` or `--help` arguments
"""

import argparse

import yaml


def main(fl):
    rngs = False
    cfg = yaml.load(fl, yaml.Loader)
    separate = cfg[0]["separate"]
    del cfg[0]
    for cfg_element in cfg:
        scanfile = cfg_element["file"]
        all_scans = get_scans(scanfile)
        scan_numbers_tmp = cfg_element["scan"]
        col_numbers = cfg_element["column"]

        scan_numbers = []

        for item in scan_numbers_tmp:
            if type(item) is str and "-" in item:
                low, up = [int(x.replace(" ", "")) for x in item.split("-")]
                a = range(low, up)
                a.append(up)
                scan_numbers[scan_numbers_tmp.index(item):scan_numbers_tmp.index(item)] = a
                rngs = True
            else:
                scan_numbers.append(item)

        scans = []
        for s in all_scans:
            if get_scan_number(s) in scan_numbers:
                scans.append(s)
        del all_scans  # this list can be pretty long so deleting it asap to not use memory
        if separate:
            for s in scans:
                cols = format_columns(get_columns_by_number(get_all_columns(s), col_numbers))
                save_data(cols, scanfile, scan_numbers[scans.index(s)])
        else:
            cols = ""
            for s in scans:
                cols += format_columns(get_columns_by_number(get_all_columns(s), col_numbers)) + "\n\n"
            if rngs:
                save_data(cols, scanfile, *scan_numbers_tmp)
            else:
                save_data(cols, scanfile, *scan_numbers)


def get_scans(filename):
    if not filename.endswith(".dat"):
        filename += ".dat"
    with open(filename) as fp:
        return fp.read().split("\n\n")


def get_scan_number(scan):
    lines = scan.split("\n")
    return int(lines[0].split(" ")[1])


def get_all_columns(scan):
    lines = scan.split("\n")
    for line in lines:
        if line.startswith("#N"):
            col_number = int(line.split(" ")[1])
    cols = [list() for _ in range(col_number)]
    for line in lines:
        if line.startswith("#"):
            continue
        d = line.split(" ")
        for i in range(col_number):
            cols[i].append(d[i])
    return cols


def get_columns_by_number(cols, col_numbers):
    tmp = []
    for i in range(len(cols)):
        if i + 1 in col_numbers:
            tmp.append(cols[i])
    return tmp


def save_data(data, filename, *scan_numbers):
    if "/" in filename:
        filename = filename.split("/")[-1]
    if filename.endswith(".dat"):
        filename = filename.split(".")[0]
    filename += "_" + "_".join([str(i) for i in scan_numbers])
    filename += ".dat"

    with open(filename, "w") as fp:
        fp.write(data)


def format_columns(data):
    text = ""
    for i in range(len(data[0])):
        text += " ".join([c[i] for c in data]) + "\n"
    return text


def run():
    parser = argparse.ArgumentParser(description="Simple application to extract data from scan file")
    parser.add_argument("config_file", type=argparse.FileType("r"), help="path to configuration file")
    args = parser.parse_args()
    main(args.config_file)


if __name__ == "__main__":
    run()
